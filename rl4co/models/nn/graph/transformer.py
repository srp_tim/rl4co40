# Import necessary packages
import torch.nn as nn
from torch import Tensor
from tensordict import TensorDict
from typing import Tuple, Union
from rl4co.models.nn.env_embeddings import env_init_embedding
from torch.nn import TransformerEncoder, TransformerEncoderLayer

class BaseEncoder(nn.Module):
    def __init__(
            self,
            env_name: str,
            embedding_dim: int,
            init_embedding: nn.Module = None,
        ):
        super(BaseEncoder, self).__init__()
        self.env_name = env_name
        
        # Init embedding for each environment
        self.init_embedding = (
            env_init_embedding(self.env_name, {"embedding_dim": embedding_dim})
            if init_embedding is None
            else init_embedding
        )

    def forward(
        self, td: TensorDict, mask: Union[Tensor, None] = None
    ) -> Tuple[Tensor, Tensor]:
        """
        Args:
            td: Input TensorDict containing the environment state
            mask: Mask to apply to the attention

        Returns:
            h: Latent representation of the input
            init_h: Initial embedding of the input
        """
        init_h = self.init_embedding(td)
        h = None
        return h, init_h

class GraphTransformerEncoder(BaseEncoder):
    def __init__(
            self,
            env_name: str,
            embedding_dim: int,
            init_embedding: nn.Module = None,
            num_layers: int = 6, # number of transformer layers
            num_heads: int = 8, # number of attention heads
            dropout: float = 0.1, # dropout rate
        ):
        super(GraphTransformerEncoder, self).__init__(env_name, embedding_dim, init_embedding)
        
        # Create a transformer encoder layer
        self.transformer_layer = TransformerEncoderLayer(
            d_model=embedding_dim, # input dimension
            nhead=num_heads, # number of attention heads
            dropout=dropout # dropout rate
        )

        # Create a transformer encoder
        self.transformer = TransformerEncoder(
            encoder_layer=self.transformer_layer, # transformer layer
            num_layers=num_layers, # number of layers
            norm=None # normalization layer
        )

    def forward(
        self, td: TensorDict, mask: Union[Tensor, None] = None
    ) -> Tuple[Tensor, Tensor]:
        """
        Args:
            td: Input TensorDict containing the environment state
            mask: Mask to apply to the attention

        Returns:
            h: Latent representation of the input
            init_h: Initial embedding of the input
        """
        # Get the initial embedding of the input
        init_h = self.init_embedding(td)

        # Apply the transformer encoder to the initial embedding
        h = self.transformer(init_h, src_key_padding_mask=mask)

        return h, init_h
