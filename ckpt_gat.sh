#!/usr/bin/env bash
#SBATCH --job-name=ckptgat
#SBATCH --output=ckptgat%j.log
#SBATCH --error=ckptgat%j.err
#SBATCH --mail-user=alhermi@uni-hildesheim.de
#SBATCH --partition=STUD
#SBATCH --gres=gpu:1

#!/usr/bin/env bash


DEVICES="0"

#WANDB_MODE=dryrun
wandb login 9815f3b0e8ce08b8878d8ca9a84bca3dd70f3978
wandb offline

CUDA_VISIBLE_DEVICES="$DEVICES" python ckpt_gat.py








